package com.evolint.gmap;

import android.app.Dialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.fitness.data.MapValue;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    GoogleMap myGmap;
    MapView mMapView;
    private static final int GPS_ERRORDIALOG_ReQUELT = 1010;
    private Button go;
    private Button goR;
    private TextView info;
    private EditText location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(servicesOK()){

            setContentView(R.layout.activity_mapview);
            mMapView = (MapView)findViewById(R.id.map);
            go=(Button)findViewById(R.id.btn_go);
            goR=(Button)findViewById(R.id.btn_gorumy);
            info=(TextView)findViewById(R.id.info);
            location=(EditText)findViewById(R.id.location);
            mMapView.onCreate(savedInstanceState);
            if(initMap()){
                Toast.makeText(this,"Map ready",Toast.LENGTH_LONG).show();
                gotoLocation(22.365584,91.821531);
            }

            go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    info.setText(geoLocate(v));
                    location.setText("");
                }
            });
            goR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoLocation(22.365846,91.821821,16);
                    info.setText("Welcome to RUMY\nLatitude: 22.365846\n Longitude: 91.821821");
                }
            });



        }
        else{

            setContentView(R.layout.activity_main);
        }

    }

    public String geoLocate(View v){
        InputMethodManager im = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(v.getWindowToken(),0);
        String loc = location.getText().toString();
        Geocoder gc = new Geocoder(this);

        try {
            List<Address> list = gc.getFromLocationName(loc ,1);

                Address add = list.get(0);
                gotoLocation(add.getLatitude(),add.getLongitude(),16);
                return "Showing location: "+add.getLocality()+"\nLatitude: "+add.getLatitude()+"\nLongitude: "+add.getLongitude();

        } catch (Exception e) {
            Toast.makeText(this,"location not available",Toast.LENGTH_LONG).show();
        }
        return "No valid address found";

    }

    public boolean servicesOK(){
        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(isAvailable== ConnectionResult.SUCCESS){
            return true;

        }
        else if(GooglePlayServicesUtil.isUserRecoverableError(isAvailable)){
            Dialog dialog =GooglePlayServicesUtil.getErrorDialog(isAvailable,this,GPS_ERRORDIALOG_ReQUELT);
            dialog.show();
        }
        else {
            Toast.makeText(this,"Cant connect to google play services",Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mMapView.onSaveInstanceState(outState);
    }

    private void gotoLocation(double latitude,double longitude){
        LatLng latlong= new LatLng(latitude,longitude);
        CameraUpdate update = CameraUpdateFactory.newLatLng(latlong);
        myGmap.moveCamera(update);
    }
    private void gotoLocation(double latitude,double longitude,float zoom){
        LatLng latlong= new LatLng(latitude,longitude);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latlong, zoom);
        myGmap.animateCamera(update);
    }

    private boolean initMap(){
        if (myGmap==null){
           myGmap = mMapView.getMap();
        }

        return  (myGmap!=null);
    }
}
